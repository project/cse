<?php

/**
 * Menu callback; presents the cse admin settings page.
 */

function cse_admin_settings() {
  $form['cse_policy'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSE policy'),
    '#collapsible' => TRUE,
    '#weight' => 0,
  );
  $form['cse_policy']['cse_reader_cookies'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable viewer cookies'),
    '#default_value' => variable_get('cse_reader_cookies',1),
    '#description' => t('Viewer cookies allow a viewer to see multiple pages with the password stored as a cookie on the browser.'),
    '#weight' => 0,
  );
  $form['cse_policy']['cse_user_password'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable dynamic password strength checking'),
    '#default_value' => variable_get('cse_user_password',1),
    '#description' => t('Drupal provides javascript password strength checking, enabling this option includes the Drupal javascript code to automatically check the strength of passwords in the CSE password entry fields.'),
    '#weight' => 1,
  );
  $form['cse_policy']['cse_statusbar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable editing status bar'),
    '#default_value' => variable_get('cse_statusbar',1),
    '#description' => t('The editing status bar is a CSE editor facility showing the status of editing operations.'),
    '#weight' => 1,
  );
  $form['cse_feel'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSE appearance'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 0,
  );
  $form['cse_feel']['cse_id_text'] = array(
    '#type' => 'textfield',
    '#title' => t('CSE Identifier'),
    '#size' => 20,
    '#maxlength' => 30,
    '#default_value' => variable_get('cse_id_text','[encrypted]'),
    '#description' => t('The text used as a replacement for the parts of the document that are encrypted.  It should be a minimum of 3 characters and the use of characters "&lt;" and "&gt;" is not permitted.'),
    '#weight' => 0,
  );
  $form['cse_feel']['cse_colour'] = array(
    '#type' => 'textfield',
    '#title' => t('CSE Colour'),
    '#size' => 20,
    '#maxlength' => 30,
    '#default_value' => variable_get('cse_colour','#b94'),
    '#description' => t('The colour used to display the replacement text. The colour should start with a hash followed by three or six hexadecimal digits, e.g. #ff0000'),
    '#weight' => 1,
  );
  $form['cse_feel']['cse_edit_weight'] = array(
    '#type' => 'select',
    '#options' => array(-5=>-5,-4=>-4,-3=>-3,-2=>-2,-1=>-1,0=>0,1=>1,2=>2,3=>3,4=>4,5=>5),
    '#title' => t('CSE Utilities node weight'),
    '#default_value' => variable_get('cse_edit_weight',-1),
    '#description' => t('Weights the position of the "CSE utilities" in the node add/update form.'),
    '#weight' => 2,
  );
  $form['cse_feel']['cse_comment_weight'] = array(
    '#type' => 'select',
    '#options' => array(-5=>-5,-4=>-4,-3=>-3,-2=>-2,-1=>-1,0=>0,1=>1,2=>2,3=>3,4=>4,5=>5),
    '#title' => t('CSE Utilities comment weight'),
    '#default_value' => variable_get('cse_comment_weight',1),
    '#description' => t('Weights the position of the "CSE utilities" in the add/update comment form.'),
    '#weight' => 3,
  );

  return system_settings_form($form);
}

